import { Injectable } from '@angular/core';
// @ts-ignore
import {Album} from "../models/Album";
import {Observable, of} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AlbumsService {
  albums: Album[];

  data: Observable<any>;

  constructor() {
    this.albums = [
      {
        title: 'Music for the Masses',
        artist: 'Depeche Mode',
        songs: ['Strangelove', 'Never Let Me Down Again', 'Behind the Wheel'],
        favorite: 'Strangelove',
        year: 1987,
        genre: 'Synth-pop new wave',
        units: 1000000,
        image: "../../assets/image/DepecheMode.png",
        isActive: true
      },
      {
        title: 'Battle Studies',
        artist: 'John Mayer',
        songs: ['Heartbreak Warfare', 'Half of My Heart', 'Who Says'],
        favorite: 'Half of My Heart',
        year: 2009,
        genre: 'Pop Rock',
        units: 1100000,
        image: "../../assets/image/JohnMayer.jpg",
      },
      { title: 'V',
        artist: 'Maroon 5',
        songs: ['Sugar', 'Animals', 'Maps'],
        favorite: 'Sugar',
        year: 2014,
        genre: 'Pop Rock',
        units: 3000000,
        image: "../../assets/image/maroon5.png",
        isActive: true
      },
      {
        title: 'X&Y',
        artist: 'Coldplay',
        songs: ['Speed of Sound', 'Fix You', 'Talk'],
        favorite: 'Talk',
        year: 2005,
        genre: 'Alternative Rock',
        units: 3000000,
        image: "../../assets/image/Coldplay.png",
      },
      {
        title: 'Tailgates & Tanlines',
        artist: 'Luke Bryan',
        songs: ['Country Girl', 'Drunk on You', 'Kiss Tomorrow Goodbye'],
        favorite: 'Country Girl',
        year: 2011,
        genre: 'Country',
        units: 4000000,
        image: "../../assets/image/Luke Bryan.jpg",

        isActive: true
      },

    ];


  }

  getUsers(): Observable<Album[]> {
    // alert('Fetching users from service');
    console.log('Fetching users from service')
    return of(this.albums);
  }
  // CREATE a method that adds a new user to the array//.push - is at the end of array
  addUser(user: Album) {
    console.log('Added user from service');
    this.albums.unshift(user);
  }

}
