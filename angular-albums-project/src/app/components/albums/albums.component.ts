import {Component, OnInit, ViewChild} from '@angular/core';

import {AlbumsService} from "../../services/albums.service";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  // album : Album = {
  //   //properties
  //   title: '',
  //   artist: '',
  //   songs: [] ,
  //   genre: '',
  //   units: null,
  //   year: null,
  //   favorite: '',
  // };


  albums: Album[];
  displayInfo: boolean = true;
  enableAddAlbum: boolean;
  loadingAlbums: boolean = true;

  // CREATE A PROPERTY FOR THE OBSERVABLE
  // data: any;
  // showUserForm: boolean = false;
  // enableAddUser: boolean;
  // currentClass: {};  //basically an empty object
  // currentStyle: {};
  // @ViewChild("userForm") form: any;


  constructor(private albumsService: AlbumsService) {
  }

  ngOnInit(): void {

    this.albumsService.getUsers().subscribe(albums => {
      this.albums = albums;
    })


    // this.enableAddAlbum = true;

    // this.setCurrentClasses();
    // this.setCurrentStyle();

    // this.albums = [
    //   {
    //   title: 'Music for the Masses',
    //   artist: 'Depeche Mode',
    //   songs: ['Strangelove', 'Never Let Me Down Again', 'Behind the Wheel'],
    //   favorite: 'Strangelove',
    //   year: 1987,
    //   genre: 'Synth-pop new wave',
    //   units: 1000000,
    //   image: "../../assets/image/DepecheMode.png",
    //     isActive: true
    // },
    //   {
    //     title: 'Battle Studies',
    //     artist: 'John Mayer',
    //     songs: ['Heartbreak Warfare', 'Half of My Heart', 'Who Says'],
    //     favorite: 'Half of My Heart',
    //     year: 2009,
    //     genre: 'Pop Rock',
    //     units: 1100000,
    //     image: "../../assets/image/JohnMayer.jpg",
    //   },
    //   { title: 'V',
    //     artist: 'Maroon 5',
    //     songs: ['Sugar', 'Animals', 'Maps'],
    //     favorite: 'Sugar',
    //     year: 2014,
    //     genre: 'Pop Rock',
    //     units: 3000000,
    //     image: "../../assets/image/maroon5.png",
    //     isActive: true
    //   },
    //   {
    //     title: 'X&Y',
    //     artist: 'Coldplay',
    //     songs: ['Speed of Sound', 'Fix You', 'Talk'],
    //     favorite: 'Talk',
    //     year: 2005,
    //     genre: 'Alternative Rock',
    //     units: 3000000,
    //     image: "../../assets/image/Coldplay.png",
    //   },
    //   {
    //     title: 'Tailgates & Tanlines',
    //     artist: 'Luke Bryan',
    //     songs: ['Country Girl', 'Drunk on You', 'Kiss Tomorrow Goodbye'],
    //     favorite: 'Country Girl',
    //     year: 2011,
    //     genre: 'Country',
    //     units: 4000000,
    //     image: "../../assets/image/Luke Bryan.jpg",
    //
    //     isActive: true
    //   },
    //
    // ];

  } //END OF NGONINIT ()    DONT DELETE
  //
  // setCurrentClasses() {
  //   this.currentClass = {
  //     'btn-dark': this.enableAddAlbum,
  //   }
  // };
  //
  // setCurrentStyle(){
  //   this.currentStyle = {
  //     'padding-top': this.displayInfo ? '0' : '80px'
  //   }
  // }
  //
  // fireEvent(e) {
  //   console.log('The event has just happened');
  // }
  //
  // onSubmit({value,valid}:{value:Album, valid:boolean}) {
  //   if (!valid) {
  //     alert("Your Form is invalid!!!!")
  //   } else {
  //     value.isActive = true
  //     value.memberSince = new Date();
  //     value.hide = true;
  //
  //     this.dataService.addUser(value);
  //     this.form.reset();
  //   }
  // }

  // toggleHide(user: Album){
  //   user.hide = !user.hide;
  // }

  addAlbum(album: Album) {
    this.albums.unshift();

  }
  onSubmit(addAlbum: any){

  }

}// END OF THE CLASS


interface Album{
  title: string,
  artist: string,
  songs: string[],
  favorite: string,
  year: number,
  genre: string,
  units: number
  image?: string
  isActive?: boolean

}

